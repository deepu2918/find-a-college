**Find a Course**

## Install required dependencies
```
npm i
```

## Insert Mockdata
```
 node data-seeder/
```

## Start Application
```
npm start
```

## Test
```
npm t
```