const mongoose = require('mongoose');
const { Schema } = mongoose;

const Universities = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  country: {
    type: String,
    required: true
  },
  minimum_gpa: {
    type: Number,
    required: true
  },
  minimum_gre_score: {
    type: Number,
    required: true
  },
  status: {
    type: String,
    default: 'active',
    enum: {
      values: ['active', 'inactive'],
      message: '{VALUE} is not supported'
    }
  }
}, {
  timestamps: true
});

module.exports = mongoose.model('universities', Universities);