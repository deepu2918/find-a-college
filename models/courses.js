const mongoose = require('mongoose');
const { Schema } = mongoose;
const ObjectId = Schema.ObjectId;

const courses = new Schema({
  university_id: ObjectId,
  name: String,
  teacher_name: String,
  status: {
    type: String,
    default: 'active',
    enum: {
      values: ['active', 'inactive'],
      message: '{VALUE} is not supported'
    }
  },
}, {
  timestamps: true
});

courses.index( { name: "text"} )


module.exports = mongoose.model('courses', courses);
