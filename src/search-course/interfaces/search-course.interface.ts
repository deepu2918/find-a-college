import { Document } from 'mongoose';

export interface SearchCourse extends Document {
    readonly name: string;
    readonly description: string;
    readonly country: string;
    readonly minimum_gpa: Number,
    readonly minimum_gre_score: Number,
    readonly course: String,
    readonly teacher_name: String,
    readonly status: string;
    readonly create_at: Date;
    readonly updated_at: Date;
}