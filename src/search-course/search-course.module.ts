import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SearchCourseController } from './search-course.controller';
import { SearchCourseService } from './search-course.service';
import { CoursesSchema } from './schemas/courses.schema'

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Courses', schema: CoursesSchema }]),
  ],
  controllers: [SearchCourseController],
  providers: [SearchCourseService]
})
export class SearchCourseModule { }
