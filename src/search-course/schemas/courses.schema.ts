const mongoose = require('mongoose');
const { Schema } = mongoose;
const ObjectId = Schema.ObjectId;

export const CoursesSchema = new mongoose.Schema({
  university_id: ObjectId,
  name: String,
  teacher_name: String,
  status: {
    type: String,
    default: 'active',
    enum: {
      values: ['active', 'inactive'],
      message: '{VALUE} is not supported'
    }
  },
}, {
  timestamps: true
});
