import { Controller, Get, Res, HttpStatus,Query } from '@nestjs/common';
import { SearchCourseService } from './search-course.service';

@Controller('search-course')
export class SearchCourseController {
    constructor(private searchCourseService: SearchCourseService) { }

    structureRes = (statusCode = 200, msg = 'successfully fetched', data = {}) => {
        return {
            statusCode, msg, data
        };
    };

    // search courses
    @Get()
    async searchCourse(@Res() res, @Query() query) {
        const courses = await this.searchCourseService.searchCourse(query);
        return res.status(HttpStatus.OK).json(this.structureRes(200, 'success', { courses }));
    }
}
