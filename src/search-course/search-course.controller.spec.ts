import { Test, TestingModule } from '@nestjs/testing';
import { SearchCourseController } from './search-course.controller';
import { SearchCourseService } from './search-course.service';


const courseStub = [
  {
      "_id": "615063ed4d9aae28d84f2c7b",
      "status": "active",
      "name": "Data Science",
      "teacher_name": "Anudeep Ch",
      "university": {
          "status": "active",
          "name": "The University of Chicago",
          "description": "The University of Chicago ranks among the world's most esteemed major universities. The private institution has an enrollment of some 16,500 students, about 60% of which are graduate and professional students. The undergraduate branch offers a core liberal arts curriculum and majors in more than 50 areas. Graduate programs include the University of Chicago Law School and Booth School of Business",
          "country": "USA",
          "minimum_gpa": 9.5,
          "minimum_gre_score": 780
      }
  },
  {
      "_id": "615063ed4d9aae28d84f2c7d",
      "status": "active",
      "name": "Data Science",
      "teacher_name": "Anudeep Ch",
      "university": {
          "status": "active",
          "name": "University of Pennsylvania",
          "description": "University of Pennsylvania is a higher educational institution that offers educational services primarily for students at undergraduate, graduate, professional, and postdoctoral levels. The institution also performs research, training, and other services under grants, contracts, and similar agreements with sponsoring organizations primarily departments and agencies of the United States Government",
          "country": "USA",
          "minimum_gpa": 9,
          "minimum_gre_score": 800
      }
  },
  {
      "_id": "615063ed4d9aae28d84f2c7f",
      "status": "active",
      "name": "Data Science",
      "teacher_name": "Anudeep Ch",
      "university": {
          "status": "active",
          "name": "Georgia Institute of Technology",
          "description": "The Georgia Institute of Technology, commonly referred to as Georgia Tech or, in the state of Georgia, as Tech, is a public research university and institute of technology in Atlanta, Georgia. It is part of the University System of Georgia and has satellite campuses in Savannah, Georgia; Metz, France; Athlone, Ireland; Shenzhen, China; and Singapore. The school was founded in 1885 as the Georgia",
          "country": "USA",
          "minimum_gpa": 9,
          "minimum_gre_score": 750
      }
  }
]
describe('SearchCourseController', () => {
  let controller: SearchCourseController;

  const mockService = {
    searchCourse: jest.fn(dto => {
      return courseStub
    }),
  };

  const mockResponse = () => {
    const res = {
      status: jest.fn(res => {
        return {
          json: (res) => res,
        }
      }),
    }
    return res;
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SearchCourseController],
      providers: [SearchCourseService]
    }).overrideProvider(SearchCourseService).useValue(mockService).compile();

    controller = module.get<SearchCourseController>(SearchCourseController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });


  it('should return movies list', () => {
    const res = mockResponse();
    controller.searchCourse(res, 1).then(data => {
      expect(data).toEqual({
        "statusCode": 200,
        "msg": "success",
        "data": {
          "courses": courseStub
        }
      });
    });
  });
});
