import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { SearchCourse } from './interfaces/search-course.interface';

@Injectable()
export class SearchCourseService {

    constructor(@InjectModel('Courses') private coursesModel: Model<SearchCourse>) { }

    /**
     * 
     * @param query params can be(gpa, gre, country, course)
     * @returns courses offered by universities list.
     */
    async searchCourse(query): Promise<SearchCourse[]> {
        const uniMatch = { "$match": {} };
        if (query) {
            if (query.gpa) {
                uniMatch["$match"]["university.minimum_gpa"] = { $gte: parseInt(query.gpa) }
            }
            if (query.gre) {
                uniMatch["$match"]["university.minimum_gre_score"] = { $gte: parseInt(query.gre) }
            }
            if (query.country) {
                uniMatch["$match"]["university.country"] = query.country;
            }
        };
        const searchQuery = [];
        if (query.course) {
            searchQuery.push({
                "$match": {
                    "$text": {
                        "$search": query.course
                    }
                }
            });
        }
        searchQuery.push({
            "$lookup":
            {
                from: 'universities',
                localField: 'university_id',
                foreignField: '_id',
                as: 'university'
            }
        },
            {
                "$unwind": "$university"
            },
            { ...uniMatch },
            { $project: { status: 1, name: 1, teacher_name: 1, 'university.status': 1, 'university.name': 1, 'university.description': 1, 'university.country': 1, 'university.minimum_gpa': 1, 'university.minimum_gre_score': 1, } })
        const courses = await this.coursesModel.aggregate(searchQuery);
        return courses;
    }

}
