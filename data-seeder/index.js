require('dotenv').config({
    path: `./env-files/${process.env.NODE_ENV || 'development'}.env`,
});
const mongoose = require("mongoose");
const Universities = require("../models/universities");
const mockUniversities = require('./../mock-data/universities');
const Courses = require("../models/courses");
const mockCourses = require('./../mock-data/courses');
let dbConnection;

//insert universities and courses
const startProcess = () => {
    Universities.insertMany(mockUniversities).then(r => {
        console.log('inserted universites');
        Courses.insertMany(mockCourses).then(rc => {
            console.log('inserted courses');
            mongoose.disconnect();
        }).catch(err => {
            console.error('error while inserting courses', err);
        });
    }).catch(err => {
        console.error('error while inserting universites', err);
    });
};

// clean collections
const dropCollections = () => new Promise((resolve, reject) => {
    dbConnection.db.listCollections().toArray((err, names) =>{
        if (err) {
            console.error(err);
        } else {
            if (names.length > 0) {
                Universities.collection.drop();
                Courses.collection.drop();
                resolve('collections dropped');
            } else {
                resolve('no collections available to drop');
            }
        }
    });
});

//connect mongoose
mongoose
    .connect(process.env.MONGODB_URI, { useNewUrlParser: true })
    .catch(err => {
        console.error('error while connecting mongodb', err.stack);
        process.exit(1);
    })
    .then(() => {
        dbConnection = mongoose.connection;
        console.log("connected to db in development environment");
        dropCollections().then(() => {
            console.log('collection dropped');
            console.log('starting inserting mock-data');
            startProcess();
        });
    });

